#include <iostream>
#include <math.h>
using namespace std;

int main() {
    double a,b,c,d,x1,x2;
    cout <<"a:"; cin >> a;
    cout <<"b:"; cin >> b;
    cout <<"c:"; cin >> c;

    if (a == 0.0){
        cout << "This is not a quadratic equation";
        return 0;
    }

    d = b*b - 4*a*c;
    cout << "D:" << d << "\n";
    if (d > 0.0){
        x1 = ((-b + sqrt(d))/(2*a));
        x2 = ((-b - sqrt(d))/(2*a));
        cout << "x1:" << x1 <<"\n" <<"x2:" << x2;
    } else if (d == 0.0){
        x1 = (-b + sqrt(d))/2*a;
        x2 = x1;
        cout << "x1 and x2" << x1;
    } else{
        cout << "ERROR";
    }

    return 0;

}
