#include <iostream>
using namespace std;

int power(int x, unsigned p)
{
    int i;
    int result = 1;
    for (i = 1; i <= p; i++)
        result = result * x;
    return result;
}

int main() {
    int x;
    unsigned p;
    cout << "Number:";
    cin >> x;
    cout << "Power:";
    cin >> p;
    cout << "Result:" << power(x, p);
}