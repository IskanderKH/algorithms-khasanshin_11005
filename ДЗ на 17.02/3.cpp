#include <iostream>
using namespace std;

int main() {
    int x;
    cout << "Number:";
    cin >> x;

    int p = 0;
    int power_2 = 1;

    while (power_2 <= x){
        p += 1;
        power_2 *= 2;
    }
    cout << "max p: " << p - 1;

}