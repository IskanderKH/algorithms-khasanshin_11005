// Склеивает массив
#include <iostream>
using namespace std;

int main(){
    int k,n,m;
    cout << "Enter the length of A array:"; cin >> n;
    cout << "Enter the length of B array:"; cin >> m;
    cout << endl;
    int * a = new int[n];
    cout << "Enter the elements of array A:";
    for (int i = 0; i < n; i++){
        cin >> a[i];
    }

    int * b = new int[m];
    cout << "Enter the elements of array B:";
    for (int i = 0; i < m; i++){
        cin >> b[i]; 
    }
    cout << endl;

    k = n + m;
    int * c = new int[k];
    for(int i = 0; i < n; i++) {
        c[i] = a[i];
    }
    for(int i = 0; i < m; i++){
        c[i+n] = b[i];
    }

    delete[] a;
    delete[] b;

    cout << "Array C:";
    for(int i = 0; i < k; i++){
        cout << " " << c[i];
    }


    return 0;
}