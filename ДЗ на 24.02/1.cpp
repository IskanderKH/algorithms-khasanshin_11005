#include <iostream>
using namespace std;

int sum(int * a, int size){
    int sum = 0;
    for (int i = 0; i < size; i++){
        sum += a[i];
    }
    return sum;

}


int main(){
    int const size = 5;
    int a[size];
    cout << "Enter " << size << " array elements:";
    for(int i = 0; i < size; i++){
        cin >> a[i];
    }
    int * p = &a[0];

    cout << "Sum:" <<  sum(p, size);  

    return 0;
}